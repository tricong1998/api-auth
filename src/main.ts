import * as dotenv from 'dotenv';
dotenv.config();
import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger } from 'nestjs-pino';
import { AppModule } from './modules/app/app.module';
import { createLightship } from 'lightship';
import * as config from 'config';
async function bootstrap() {
  console.log(config);
  const app = await NestFactory.create(AppModule, {
    cors: true,
    bodyParser: true,
  });

  const logger = app.get(Logger);

  await initializeApp(app, logger);
  await setupSwagger(app);
  await app.listen(3000);
}

async function initializeApp(app: INestApplication, logger: Logger) {
  initializeHttpContext(app); // mush be first
  app.useLogger(logger);
  app.enableCors({
    exposedHeaders: config.get('server.cors.exposedHeaders'),
    allowedHeaders: config.get('server.cors.allowedHeaders'),
  });
  app.use(responseTime({ header: 'x-response-time' }));
  app.use((req: express.Request, res: express.Response, next: () => void) => {
    const correlationId = uuidV4();
    httpContext.set(HTTP_CONTEXT.TIMESTAMP, Date.now());
    httpContext.set(HTTP_CONTEXT.CORRELATION_ID, correlationId);
    req.id = correlationId;
    next();
  });
  app.useGlobalFilters(new HttpExceptionFilter(logger));
  app.useGlobalInterceptors(new ResponseBodyLoggingInterceptor(logger));
  app.useGlobalPipes(
    new AppValidationPipe({
      transform: true,
      validationError: {
        target: false,
        value: false,
      },
      whitelist: true,
    }),
  );
  app.setGlobalPrefix(config.get('service.baseUrl'));
}

async function initializeLightship(app: INestApplication) {
  const lightship = createLightship();

  lightship.registerShutdownHandler(async () => {
    await app.close();
  });

  return lightship;
}

bootstrap().then((app) => {
  const logger = app.get(Logger);
  logger.log(
    `${config.get<string>('service.name')} is ready on ${config.get<string>(
      'server.hostname',
    )}${config.get<string>('service.baseUrl')}`,
  );
  logger.log(
    `${config.get<string>('service.name')} docs is ready on ${config.get<string>(
      'server.hostname',
    )}${config.get<string>('service.docsBaseUrl')}`,
  );
});


async function setupSwagger(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('Cats example')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .addTag('cats')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
}

(async () => {
  const app = await bootstrap();
})();
