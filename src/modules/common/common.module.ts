import { Global, Module } from '@nestjs/common';
import { coreLoggerModule } from './logger/coreLogger';
import { coreMongoModule } from './mongo/coreMongo';

@Global()
@Module({
  imports: [coreMongoModule, coreLoggerModule],
})
export class CommonModule {}
