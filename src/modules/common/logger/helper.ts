import { Params } from 'nestjs-pino';
import pino from 'pino';

export const getLoggerConfig = async (): Promise<Params> => ({
  pinoHttp: {
    level: process.env.NODE_ENV !== 'production' ? 'debug' : 'info',
    serializers: {
      err: pino.stdSerializers.err,
      req: (req) => {
        req.body = req.raw.body;
        return req;
      },
    },
  },
  exclude: [],
});
