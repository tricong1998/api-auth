import { LoggerModule } from 'nestjs-pino';
import { getLoggerConfig } from './helper';

export const coreLoggerModule = LoggerModule.forRootAsync({
  useFactory: async () => getLoggerConfig(),
});
