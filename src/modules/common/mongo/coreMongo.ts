import { MongooseModule } from '@nestjs/mongoose';
import { getMongoConfig } from './helper';

export const coreMongoModule = MongooseModule.forRootAsync({
  useFactory: () => getMongoConfig(),
});
