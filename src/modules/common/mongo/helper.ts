import * as config from 'config';
import { MongooseModuleOptions } from '@nestjs/mongoose';

console.log(config.get('mongodb.uri'));

export const getMongoConfig = (): MongooseModuleOptions => ({
  uri: config.get('mongodb.uri'),
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
});
