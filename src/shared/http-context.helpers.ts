import { INestApplication } from '@nestjs/common';
import express from 'express';
import httpContext from 'express-http-context';

export const initializeHttpContext = (app: INestApplication) => {
  // for work with GET method
  app.use(httpContext.middleware);

  // for work with other methods
  app.use((req: express.Request, res: express.Response, next) => {
    httpContext.ns.bindEmitter(req);
    httpContext.ns.bindEmitter(res);
    next();
  });
};
