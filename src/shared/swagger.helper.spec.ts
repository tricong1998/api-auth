import { DiscoveredMethodWithMeta, DiscoveryService } from '@golevelup/nestjs-discovery';
import { Type } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { IamNamespaces, IamPermissions } from './constants';
import { injectEndpointPolicyToSwaggerMetadata } from './swagger.helper';
import { DECORATORS } from '@nestjs/swagger/dist/constants';

describe('swagger helper', () => {
  describe('injectEndpointPolicyToSwaggerMetadata()', () => {
    const mockEndpoint = {
      meta: [
        [
          {
            namespace: IamNamespaces.SETEL_CUSTOMER,
            permissions: [IamPermissions.PUBLIC_ACCESS],
          },
        ],
      ],
      discoveredMethod: {
        handler: jest.fn(),
        methodName: 'helloWorld',
        parentClass: {} as any,
      },
    };
    const mockDiscoveryService: any = {
      methodsAndControllerMethodsWithMetaAtKey: jest.fn((): DiscoveredMethodWithMeta<any>[] => [
        mockEndpoint,
      ]),
    };
    const mockReflector: any = {
      get: jest.fn(() => ({
        description: 'Hello world',
      })),
    };
    const mockApp: any = {
      get: jest.fn().mockImplementation((type: Type<DiscoveryService> | Type<Reflector>):
        | DiscoveryService
        | Reflector => {
        if (type === DiscoveryService) {
          return mockDiscoveryService;
        } else {
          return mockReflector;
        }
      }),
    };

    it('should rewrite description in the api operation metadata', async () => {
      const reflectSpy = jest.spyOn(Reflect, 'defineMetadata');
      await injectEndpointPolicyToSwaggerMetadata(mockApp);
      expect(reflectSpy).toBeCalledWith(
        DECORATORS.API_OPERATION,
        {
          description: `Hello world<hr/>


**Endpoint policy**

**${IamNamespaces.SETEL_CUSTOMER}:** ${IamPermissions.PUBLIC_ACCESS}<br/>`,
        },
        mockEndpoint.discoveredMethod.handler,
      );
    });
  });
});
